export interface ICandidateDTO {
  name: string;
  email: string;
  skills: string;
}

export interface ICandidate {
  id: number;
  name: string;
  email: string;
  skills: string;
  createdAt: Date;
}
