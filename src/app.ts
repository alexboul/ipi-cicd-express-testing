import express from 'express';
import morgan from 'morgan';
import dotenv from 'dotenv';
import apiRouter from './routes/api';

// Load environment variables from .env file
dotenv.config();

const app = express();

// Middlewares
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Routeur
app.use('/api', apiRouter);

// Autres routes (à supprimer éventuellement)
app.get('/', (req, res) => {
  if (req.query.name !== undefined) {
    const name = JSON.parse(JSON.stringify(req.query.name));
    return res.send({ message: `Hello, ${name}!` });
  } else {
    res.send({ message: 'Hello, World!' });
  }
});

export default app;
