import express from 'express';
import sampleModel from '../models/sample';

const router = express.Router();
router.post('/', async (req, res) => {
  if (!req.body.name) {
    res.sendStatus(400);
  } else {
    await sampleModel.create({ name: req.body.name });
    res.sendStatus(204);
  }
});

router.get('/', async (req, res) => {
  const samples = await sampleModel.getAll();
  res.status(200).json(samples).send();
});

router.get('/:id', async (req, res) => {
  if (!req.params.id) {
    res.status(404).send({ message: 'No ID' });
  } else {
    const id = Number(req.params.id);
    if (isNaN(id)) {
      res.status(400).send({ message: 'Invalid id' });
    } else {
      const sample = await sampleModel.getOneById(id);
      if (sample) {
        res.status(200).json(sample).send();
      } else {
        res.status(404).send({ message: 'Sample not found' });
      }
    }
  }
});

router.put('/:id', async (req, res) => {
  if (!req.params.id) {
    res.status(404).send({ message: 'No ID' });
  } else {
    const id = Number(req.params.id);
    if (isNaN(id)) {
      res.status(400).send({ message: 'Invalid id' });
    } else {
      if (!req.body.name) {
        res.status(400).send({ message: 'No name' });
      } else {
        const sample = await sampleModel.updateOne(id, { name: req.body.name });
        if (sample) {
          res.status(200).json(sample).send();
        } else {
          res.status(404).send({ message: 'Sample not found' });
        }
      }
    }
  }
});

export default router;
