import app from '../src/app';
import { queryAsync } from '../src/db';
import sampleModel, { ISampleDTO } from '../src/models/sample';
import request from 'supertest';

async function resetSampleTable() {
  await queryAsync('DELETE FROM sample');
  await queryAsync("UPDATE sqlite_sequence SET seq = 0 WHERE name = 'sample'");
}

describe('sample API', () => {
  beforeEach(async () => {
    await resetSampleTable();
  });

  describe('create', () => {
    it('should create a sample with the correct name', async () => {
      const sample: ISampleDTO = { name: 'Sample name' };

      const res = await request(app).post('/api/samples/').send(sample);

      expect(res.status).toBe(204);
    });

    it('should not create a sample with an empty name', async () => {
      const sample: ISampleDTO = { name: '' };

      const res = await request(app).post('/api/samples/').send(sample);

      expect(res.status).toBe(400);
    });
  });

  describe('getAll', () => {
    it('should get all samples', async () => {
      await Promise.all([
        sampleModel.create({ name: 'sample1' }),
        sampleModel.create({ name: 'sample2' }),
      ]);

      const res = await request(app).get('/api/samples/');

      expect(res.status).toBe(200);
      expect(res.body).toEqual(
        expect.arrayContaining([
          expect.objectContaining({ id: expect.any(Number), name: 'sample1' }),
          expect.objectContaining({ id: expect.any(Number), name: 'sample2' }),
        ])
      );
    });
  });

  describe('getOneById', () => {
    it('should get a sample if parameter is a valid id', async () => {
      const sample = await sampleModel.create({ name: 'sample' });

      const res = await request(app).get(`/api/samples/${sample.id}`);

      expect(res.status).toBe(200);
      expect(res.body).toEqual({ id: sample.id, name: 'sample' });
    });

    it('should not get a non-existent sample', async () => {
      const res = await request(app).get('/api/samples/1111111');

      expect(res.status).toBe(404);
    });
  });

  describe('update', () => {
    it('should update a sample if parameter is a valid id and has a name', async () => {
      const sample = await sampleModel.create({ name: 'Initial' });

      const res = await request(app)
        .put(`/api/samples/${sample.id}`)
        .send({ name: 'Updated' });

      expect(res.status).toBe(200);
      expect(res.body).toEqual({ id: sample.id, name: 'Updated' });
    });

    it('should not update a sample if parameter is a valid id but has no name', async () => {
      const sample = await sampleModel.create({ name: 'Initial' });

      const res = await request(app)
        .put(`/api/samples/${sample.id}`)
        .send({ name: '' });

      expect(res.status).toBe(400);
    });
    it('should not update a non-existent sample', async () => {
      const res = await request(app)
        .put('/api/samples/1111111')
        .send({ name: 'Updated' });

      expect(res.status).toBe(404);
    });
  });
});
